const { response } = require('express')
const express = require('express')
const cors = require('cors');

const oracleDBConnection = require('./db/dbConnections/oracleConnection')
const citiesController = require('./controllers/citiesController')
const patientsController = require('./controllers/patientsController')

const statusCode = require('./errors/errorCodes');

const app = express()
const port = 3000

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/patiensWithinRadiusFromPoint', (req, res) => {
  patientsController.getPatiensWithinRadiusFromPointController(req, res)
    .then(result => res.send(result))
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.post('/patiensWithinPolygon', (req, res) => {
  patientsController.getPatientWithinPolygonController(req, res)
    .then(result => res.send(result))
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.post('/addPatient', (req, res) => {
  patientsController.addPatientController(req, res)
    .then((result) => { res.status(201); res.send(result); })
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.get('/citiesName', (req, res) => {
  citiesController.citiesName(req, res)
    .then(result => res.send(result))
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.get('/patientsBetweenDates', (req, res) => {
  patientsController.patientsBetweenDatesController(req, res)
    .then(result => res.send(result))
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.get('/CitiesInfo', (req, res) => { 
  citiesController.citiesInfoController(req, res)
    .then(result => res.send(result))
    .catch((err) => { res.status(statusCode[err.name]); res.send(err.message); });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
  oracleDBConnection.oracleConnection();
})

module.exports = app;