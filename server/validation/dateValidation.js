module.exports.isDateRangeVaild = (inputStartDate, inputEndDate) => {
    try {
        let startDate = new Date(inputStartDate);
        let endDate = new Date(inputEndDate);

        return startDate <= endDate;
    } catch (err) {
        throw err;
    }
};