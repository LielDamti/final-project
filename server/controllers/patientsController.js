const { query } = require('express');
const service = require('../services/patientsService');
const serverError = require('../errors/serverError');
const badRequestError = require('../errors/badRequest');

module.exports.patientsBetweenDatesController = async (req, res) => {
    let inputStartDate = req.query.startDate;
    let inputEndDate = req.query.endDate;

    if(inputStartDate === undefined || inputEndDate === undefined) {
        throw new badRequestError("One of dates is not exist in url parameters");
    }

    try {
        return await service.patientsBetweenDates(inputStartDate, inputEndDate);
    } catch (err) {
        throw new serverError(err.message);
    }
}

module.exports.addPatientController = async (req, res) => {    
    let id = req.body.id;
    let location = req.body.location;
    let cityName = req.body.city_name;
    let hasSymptoms = req.body.has_symptoms;
    let severity = req.body.severity;

    console.log(location);

    if (id === undefined || location === undefined || cityName === undefined 
        || hasSymptoms === undefined || severity === undefined) {
        throw new badRequestError("One of the params does not exist in request body");
    }

    try {
        await service.addPatient(id, location, cityName, hasSymptoms, severity);
        return "patient was added successfully";
    } catch (err) {
        throw new serverError(err.message);
    }
}

module.exports.getPatientWithinPolygonController = async (req, res) => {
    let polygon = req.body.polygon;

    if (polygon === undefined) {
        throw new badRequestError("Polygon does not sent in request body");
    }

    try {

        return await service.getPatientsWithinPolygon(polygon);
    } catch (err) {
        throw new serverError(err.message);
    }
}

module.exports.getPatiensWithinRadiusFromPointController = async (req, res) => {
    let point = req.body.point;
    let radius = req.body.radius;

    if (point === undefined || radius === undefined){
        throw new badRequestError("Point or radius not exist in request body");
    }

    try {
        return await service.getPatiensWithinRadiusFromPoint(point, radius);
    } catch (err) {
        throw new serverError(err.message);
    }
}