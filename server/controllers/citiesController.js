const service = require('../services/citiesService')
const serverError = require('../errors/serverError');

module.exports.citiesInfoController = async (req, res) => {
    try {
        return await service.citiesInfo();
    } catch (err) {
        throw new serverError(err.message);
    }
}

module.exports.citiesName = async (req, res) => {
    try {
        return await service.citiesName();
    } catch (err) {
        throw new serverError(err.message);
    }
}