const oracleDBConnection = require('../db/dbConnections/oracleConnection')
const sqlQueries = require('../db/queries/sqlQueries')

module.exports.citiesInfo = async () => {
    return await oracleDBConnection.executeSql(sqlQueries.citiesInfo());
};

module.exports.citiesName = async () => {
    return await oracleDBConnection.executeSql(sqlQueries.citiesName());
};