const oracleDBConnection = require('../db/dbConnections/oracleConnection');
const sqlQueries = require('../db/queries/sqlQueries');
const dateValidation = require('../validation/dateValidation');
const badRequestError = require('../errors/badRequest');

let min_severity = 1;
let max_severity = 4;

let point_params_amount = 3;
let point_type = 2001;
let sdo_srid = 8307;
let x_index = 0;
let y_index = 1;

module.exports.patientsBetweenDates = async (inputStartDate, inputEndDate) => {
    startDate = DateCasting(inputStartDate);
    endDate = DateCasting(inputEndDate);
  
    if (!dateValidation.isDateRangeVaild(startDate, endDate)) {
        throw new badRequestError("Dates range invalid")
    }

    return await oracleDBConnection.executeSql(sqlQueries.patientsBetweenDates(inputStartDate, inputEndDate));
};

const DateCasting = (date) => {
    const year = 2;
    const month = 1;
    const day = 0;

    let splitDate = date.split('-');

    try {
        return new Date(splitDate[year], splitDate[month], splitDate[day]);
    } catch (err) {
        throw new badRequestError(`Date format of ${date} is not valid`)
    }
}

module.exports.addPatient = async (id, location, cityName, hasSymptoms, severity) => {
    let cityId = await getCityIdByCityName(cityName);

    if (hasSymptoms < 0 || hasSymptoms > 1){
        throw new badRequestError('Has symptoms must to be 1 or 0');
    }

    if (severity < min_severity || severity > max_severity){
        throw new badRequestError(`Severity must to be in range ${min_severity} - ${max_severity}`);
    }

    await oracleDBConnection.executeSql(sqlQueries.updatePatientEndDate(id));
    await oracleDBConnection.executeSql(sqlQueries.updatePotentialPatientEndDate(id));
    await oracleDBConnection.executeSql(sqlQueries.addPatient(id, createPointGeoSDO(location), cityId, hasSymptoms, severity));
    await oracleDBConnection.executeSql(sqlQueries.commit());
};

const getCityIdByCityName = async (cityName) => {
    let cityId;

    try {
        let response = await oracleDBConnection.executeSql(sqlQueries.cityIdByCityName(cityName))
        cityId = response.rows[0][0];
    } catch (err) {
        throw new badRequestError(`City name ${ cityName } is not valid`);
    }

    return  cityId;
};

module.exports.getPatientswithinPolygon = async (polygon) => {
    return await oracleDBConnection.executeSql(sqlQueries.patientsWithinPolygon(polygon));
};

module.exports.getPatiensWithinRadiusFromPoint = async (point, radius) => {
    return await oracleDBConnection.executeSql(sqlQueries.patientsWithinRadiusFromPoint(createPointGeoSDO(point), radius));
};

createPointGeoSDO = (point) => {
    if (point.length !== point_params_amount) {
        throw new badRequestError("point is not containe X or Y or Z");
    }

    return `MDSYS.SDO_GEOMETRY(${point_type}, ${sdo_srid}, MDSYS.SDO_POINT_TYPE(${point[x_index]}, ${point[y_index]}, NULL), NULL, NULL)`;
}