//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const oracleDBConnection = require('../db/dbConnections/oracleConnection')
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../index');
let should = chai.should();

chai.use(chaiHttp);

describe('App', () => {
/*
  * Test the /GET route
  */
  beforeEach(async ()=> {await oracleDBConnection.oracleConnection();});
  describe('/GET patients between dates', () => {
      it('should GET patients between two specified dates', (done) => {
        chai.request(app)
            .get('/patientsBetweenDates?startDate=01-05-2021&endDate=27-05-2021')
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('rows');
              res.body.rows.should.be.a('array');
              res.body.rows.length.should.be.eql(7);
              done();
            });
      });
      it('should return Error for missing date', (done)=>{
        chai.request(app)
        .get('/patientsBetweenDates?startDate=01-05-2021')
        .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a('object');
        res.text.should.be.equal("One of dates is not exist in url parameters");
        done();
        });
      });
      it('should return Error for wrong date format', (done)=>{
        chai.request(app)
        .get('/patientsBetweenDates?startDate=01-05-2021&endDate=27-05')
        .end((err, res) => {
        res.should.have.status(500);
        res.body.should.be.a('object');
        res.text.should.be.equal("Dates range invalid");
        done();
    });
  });
});

  describe('/GET cities', () => {

      it('should get all cities', (done)=>{
        chai.request(app)
        .get('/citiesInfo')
        .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('rows');
              res.body.rows.should.be.a('array');
              res.body.rows.length.should.be.eql(9);
        done();
        });
      });
  });
  describe('/GET cities names', () => {

    it('should get all cities names', (done)=>{
      chai.request(app)
      .get('/citiesName')
      .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property('rows');
            res.body.rows.should.be.a('array');
            res.body.rows.length.should.be.eql(9);
      done();
      });
    });
});

describe('/POST add new patient', () => {
  beforeEach(async ()=> {
    await oracleDBConnection.executeSql("DELETE FROM patients WHERE citizen_id = 290258966");
  });
  it('should add new patient', (done)=>{
    let patient = {
      id: 290258966,
      location: null,
      city_name: 'Haifa',
      has_symptoms: 0,
      severity: 1
  }
    chai.request(app)
    .post('/addPatient')
    .send(patient)
    .end((err, res) => {
      should.equal(err, null);
      res.should.have.status(201);
      res.text.should.be.equal("patient was added successfully");
    done();
    });
  });
  it('should give error for missing parameter', (done)=>{
    let patient = {
      id: 290258966,
      city_name: 'Haifa',
      has_symptoms: 0,
      severity: 1
  }
    chai.request(app)
    .post('/addPatient')
    .send(patient)
    .end((err, res) => {
      should.equal(err, null);
      res.should.have.status(400);
      res.text.should.be.equal("One of the params does not exist in request body");
    done();
    });
  });





});


});
