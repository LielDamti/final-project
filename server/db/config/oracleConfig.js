testDB = {
    user          : "testEnv",
    password      : "123456",
    connectString : "127.0.0.1:1521/XE"
};

devDB = {
    user          : "corona_info",
    password      : "123456",
    connectString : "127.0.0.1:1521/XE"
};

module.exports = { dev: devDB, test: testDB };