const oracledb = require('oracledb');
const oracleConfig = require('../config/oracleConfig.js');
const serverError = require('../../errors/serverError');

let connection;

module.exports.oracleConnection = async function oracleConnection() {
    try { 
        if(process.env.NODE_ENV==='test'){
            connection = await oracledb.getConnection(oracleConfig[process.env.NODE_ENV]);
        } else {
            connection = await oracledb.getConnection(oracleConfig['dev']);
        }
    } catch (err) {
        throw err;
    } finally {
        if (this.connection) {
            try {
                await connection.close();
            } catch (err) {
                throw err;
            } 
        }
    } 
}

module.exports.executeSql = async function executeSql(sqlCommand) {
    try {
        return await connection.execute(sqlCommand);
    } catch (err) {
        throw new serverError(err.message);
    }
}
