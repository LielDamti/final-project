module.exports.citiesInfo = () => {
    return 'SELECT name, sdo_util.to_geojson(polygon) polygon, COALESCE (patients_amount, 0) ' + 
        'FROM cities LEFT JOIN ( ' + 
        'SELECT count(city_id)  patients_amount, city_id ' +
        'FROM ( ' +
            'SELECT id, citizens.city_id ' + 
            'FROM patients LEFT JOIN citizens ' + 
            'ON citizen_id = id and end_date is null ' +
        ') GROUP BY city_id ' + 
    ') ON id = city_id'
};

module.exports.patientsBetweenDates = (startDate, endDate) => {
    return `SELECT id, first_name, last_name, cast (sdo_util.to_geojson(location_coord) as varchar2(500)) point ` + 
        `FROM ( ` + 
            `SELECT citizen_id  ` + 
            `FROM patients ` + 
            `WHERE TO_DATE('${startDate}', 'DD-MM-YYYY') <= start_date and ` +  
            `TO_DATE('${endDate}', 'DD-MM-YYYY') >= start_date ` + 
        `) left join citizens on citizen_id = id`;
};

module.exports.addPatient = (id, location, city_id, has_symptoms, severity) => {
    return `INSERT INTO patients ` + 
    `(CITIZEN_ID ,LOCATION_COORD ,CITY_ID ,START_DATE ,END_DATE ,HAS_SYMPTOMS,SEVERITY,END_TYPE_ID) `+
    `values (${id}, ${location}, ${city_id}, sysdate, null, ${has_symptoms}, ${severity}, null)`;
};

module.exports.updatePatientEndDate = (id) => {
    return `UPDATE patients ` + 
    `SET end_date = sysdate `+
    `WHERE citizen_id = ${id} and end_date is null`;
};

module.exports.updatePotentialPatientEndDate = (id) => {
    return `UPDATE potential_patients ` + 
    `SET end_date = sysdate `+
    `WHERE citizen_id = ${id} and end_date is null`;
};

module.exports.commit = () => {
    return `COMMIT`;
};

module.exports.cityIdByCityName = (city_name) => {
    return `SELECT id 
        FROM cities
        WHERE name = '${city_name}'`;
};

module.exports.citiesName = () => {
    return "SELECT name FROM cities";
}

module.exports.patientsWithinPolygon = (polygon) => {
    return `SELECT id, first_name, last_name, sdo_util.to_geojson(location_coord) location_coord  ` + 
        `FROM ( ` + 
            `SELECT citizen_id
                FROM patients
                WHERE sdo_inside(location_coord, sdo_util.from_geojson(${polygon})) = 'TRUE'` + 
        `) left join citizens on citizen_id = id`;
}

module.exports.patientsWithinRadiusFromPoint = (point, radius) => {
    return `SELECT id, first_name, last_name, cast (sdo_util.to_geojson(location_coord) as varchar2(500)) location_coord ` + 
        `FROM ( ` + 
            `SELECT citizen_id
                FROM patients
                WHERE sdo_within_distance(location_coord, ${point}, 'distance=${radius}') = 'TRUE'` +
        `) left join citizens on citizen_id = id`;
}