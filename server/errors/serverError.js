module.exports = class serverError extends Error{
    constructor(message) {
        super(message);
        this.name = 'ServerError';
    }
}