module.exports = class badRequest extends Error{
    constructor(message) {
        super(message);
        this.name = 'BadRequest';
    };
}