import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export default function ComboBox(props) {
    return (
        <Autocomplete
            onChange={props.onChange}
            options={props.options}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} required label={props.label} variant="outlined" />}
        />
    );
}