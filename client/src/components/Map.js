import React from 'react'
import Leaflet from 'leaflet';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents } from 'react-leaflet'
import './Map.css';
import 'leaflet/dist/leaflet.css';
import AddPatientDialog from './AddPatientDialog';
import ShowPatientByDateDialog from './ShowPatientsByDateDialog';
import MapToolsDrawer from './MapToolsDrawer';
import Dialog from '@material-ui/core/Dialog';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
const handler = require('../RequestHandler');
const serverUrl = "http://localhost:3000";


delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png').default,
    iconUrl: require('leaflet/dist/images/marker-icon.png').default,
    ShadowUrl: require('leaflet/dist/images/marker-shadow.png').default
})


function LocationMarker(props) {
    const [position, setPosition] = React.useState(null);
    const [openAddPatient, setOpenAddPatient] = React.useState(false);
    const [openAlertDialog, setOpenAlertDialog] = React.useState(false);
    const [isErrorStatus, setIsErrorStatus] = React.useState(false);
    const [alertContent, setAlertContent] = React.useState("default");

    const handleAddPatientClose = () => {
        setOpenAddPatient(false);
    }

    const handleAddPatientOpen = () => {
        setOpenAddPatient(true);
    }

    const handleAlertDialogClose = () => {
        setOpenAlertDialog(false);
    }

    const handleAlertDialogOpen = () => {
        setOpenAlertDialog(true);
    }

    const onSubmit = async (id, cityName, hasSymptoms, severity) => {
        const res = await handler.PostPatient(serverUrl, {id: id, location: [position.lat, position.lng, null], city_name: cityName,
            has_symptoms: hasSymptoms, severity: severity});


        setIsErrorStatus(res.status !== 201);
        setAlertContent(res.message);
        handleAlertDialogOpen();
    }

    useMapEvents({
        click(e) {
            props.disabled ? setPosition(null) : setPosition(e.latlng);
            console.log("point returned: " + e.latlng);
        }
    });

    return position === null ? null : (
        <Marker position={position}>
            <Popup>
                <Button variant="outlined" color="primary" onClick={handleAddPatientOpen}>Add Patient</Button>
                <AddPatientDialog open={openAddPatient} onClose={handleAddPatientClose} onSubmit={onSubmit} citiesName={props.citiesName}/>
                <Dialog open={openAlertDialog} onClose={handleAlertDialogClose}>
                    <Alert severity={isErrorStatus ? "error" : "success"} >{alertContent}</Alert>
                </Dialog>
            </Popup>
        </Marker>
    )
}

export default function Map() {
    const position = [32.06800399347061, 34.783474705725425];
    const [citiesName, setCitiesName] = React.useState([]);
    const [disableAddPatient, setDisableAddPatient] = React.useState(true);
    const [showDateDialog, setShowDateDialog] = React.useState(false);
    const [markers, setMarkers] = React.useState([]);
    React.useEffect(async () => setCitiesName(await handler.GetCitiesName("http://localhost:3000")), [])

    const onShowDateDialogSubmit = async (startDate, endDate) => {
        const res = await handler.GetPatientsBetweenDates(serverUrl, startDate, endDate);

        if (res === undefined || res === null) {
            return;
        }

        setMarkers(res.map(arr => {
            const point = JSON.parse(arr[3]).coordinates;
            console.log(point);
            return (
                <Marker position={point}>
                    <Popup>
                        Name: {arr[1] + " " + arr[2]}, ID: {arr[0]}
                    </Popup>
                </Marker>
            );
        }));

        console.log(markers);
    }

    return (
        <MapContainer className="MapDisplay" center={position} zoom={13}>
            <TileLayer
            attribution='&copy; <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Map</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <LocationMarker citiesName={citiesName} disabled={disableAddPatient}/>
            <MapToolsDrawer disableAddPatient={disableAddPatient} setDisableAddPatient={setDisableAddPatient}
                            showDateDialog={showDateDialog} setShowDateDialog={setShowDateDialog}/>
            <ShowPatientByDateDialog open={showDateDialog} onClose={() => setShowDateDialog(false)}
                            onSubmit={onShowDateDialogSubmit}/>
            {markers}
        </MapContainer>
    );
}