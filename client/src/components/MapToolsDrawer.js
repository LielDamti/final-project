import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MarkerIcon from '@material-ui/icons/Room';
import DateRangeIcon from '@material-ui/icons/DateRange';

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));

export default function MapToolsDrawer(props) {
    const classes = useStyles();

    function disableAll() {
        props.setDisableAddPatient(true);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
            anchor="right"
            >
            <div className={classes.toolbar} />
            <Divider/>
            <List>
                <ListItem button onClick={() => {
                    const old = props.disableAddPatient;
                    disableAll();
                    props.setDisableAddPatient(!old);
                }}>
                    <ListItemIcon><MarkerIcon/></ListItemIcon>
                    <ListItemText primary={'Add Patient'} />
                </ListItem>

                <ListItem button onClick={() => {
                    disableAll();
                    props.setShowDateDialog(true);
                }}>
                    <ListItemIcon><DateRangeIcon/></ListItemIcon>
                    <ListItemText primary={'Show Patients By Date'} />
                </ListItem>
            </List>
            </Drawer>
        </div>
    );
}
