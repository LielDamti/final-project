import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Combobox from './Combobox';
import Switch from '@material-ui/core/Switch';

export default function AddPatientDialog(props) {
    const { onClose, open } = props;
    const [formInput, setFormInput] = React.useState({ id: "", cityName: "", hasSymptoms: false, severity: "0" });
    const severity_max_input = 4;
    const id_max_length = 9;
    const [isIdInvalid, setIsIdInvalid] = React.useState(false);
    // const [isCityNameEmpty, setIsCityNameEmpty] = React.useState(false);
    // const [isSeverityEmpty, setIsSeverityEmpty] = React.useState(false);

    let severityOptions = [];
    for (let i = 1; i <= severity_max_input; i++) {
        severityOptions.push(`${i}`);
    }

    const handleClose = () => {
        onClose();
    };

    return (
        <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Add Patient</DialogTitle>
            <DialogContent>
                <TextField
                    error={isIdInvalid}
                    helperText="ID must be 9 digits long"
                    value={formInput.id}
                    onChange={(e) => {
                        setFormInput({id: e.target.value, cityName: formInput.cityName, hasSymptoms: formInput.hasSymptoms, severity: formInput.severity});
                        setIsIdInvalid(e.target.value.length !== id_max_length || !(/^\d+$/.test(e.target.value)));
                    }}

                    style={{ width: 300 }}
                    id="input_id"
                    type="number"
                    margin="dense"
                    label="ID"
                    variant="outlined"
                />
                <Combobox
                    value={formInput.cityName}
                    onChange={(e, val) => setFormInput({id: formInput.id, cityName: val, hasSymptoms: formInput.hasSymptoms, severity: formInput.severity})}
                    options={props.citiesName}
                    margin="dense"
                    label="City"
                />
                <FormControlLabel 
                    control={<Switch checked={formInput.hasSymptoms}
                            onChange={(e) => setFormInput({id: formInput.id, cityName: formInput.cityName, hasSymptoms: !formInput.hasSymptoms, severity: formInput.severity})}
                            margin="dense"
                            label="Has Symptoms?"
                            />}
                    label="Has Symptoms?"
                />
                <Combobox
                    value={formInput.severity}
                    onChange={(e, val) => setFormInput({id: formInput.id, cityName: formInput.cityName, hasSymptoms: formInput.hasSymptoms, severity: val})}
                    options={severityOptions}
                    margin="dense"
                    label="Severity"
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                    handleClose();

                    // clean input
                    setFormInput({ id: "", cityName: "", hasSymptoms: false, severity: "0" });
                }} color="primary">
                    Cancel
                </Button>
                <Button onClick={() => {
                        if (isIdInvalid || formInput.id === "" || formInput.cityName === "" || formInput.cityName === null ||
                         formInput.severity === "0" || formInput.severity === null) {
                            return;
                        }

                        console.log(formInput);
                        props.onSubmit(formInput.id, formInput.cityName, String(Number(formInput.hasSymptoms)), formInput.severity);

                        handleClose();
                        
                        // clean input
                        setFormInput({ id: "", cityName: "", hasSymptoms: false, severity: "0" });
                    }} color="primary">
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
}