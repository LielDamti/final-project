import 'date-fns';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDatePicker } from '@material-ui/pickers/DatePicker/DatePicker';
import MuiPickersUtilsProvider from '@material-ui/pickers/MuiPickersUtilsProvider';

export default function ShowPatientsByDateDialog(props) {
    const { onClose, open } = props;

    const [selectedStartDate, setSelectedStartDate] = React.useState(new Date());
    const [selectedEndDate, setSelectedEndDate] = React.useState(new Date());

    const cleanInput = () => {
        setSelectedStartDate(new Date());
        setSelectedEndDate(new Date());
    }

    const handleStartDateChange = (date) => {
        setSelectedStartDate(date);
    };

    const handleEndDateChange = (date) => {
        setSelectedEndDate(date);
    }

    const handleClose = () => {
        onClose();
    };

    const formatDateString = (date) => {
        return date.getDate()  + "-" + (date.getMonth()+1) + "-" + date.getFullYear()
    }

    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogTitle>Show Patient By Date</DialogTitle>
            <DialogContent>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container justifyContent="space-between">
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="dd/MM/yyyy"
                            id="start-date-picker"
                            margin="normal"
                            label="Start Date"
                            value={selectedStartDate}
                            onChange={handleStartDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="dd/MM/yyyy"
                            id="end-date-picker"
                            margin="normal"
                            label="End Date"
                            value={selectedEndDate}
                            onChange={handleEndDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </Grid>
                </MuiPickersUtilsProvider>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                        handleClose();
                }} color="primary">
                    Cancel
                </Button>
                <Button onClick={() => {
                        props.onSubmit(formatDateString(selectedStartDate), formatDateString(selectedEndDate));

                        handleClose();
                    }} color="primary">
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
}