
async function GetCitiesName(url) {
    let res = await fetch(`${url}/citiesName`);
    let data = await res.json();

    console.log(data.rows.map(arr => arr[0]));
    return data.rows.map(arr => arr[0]);
}

async function GetPatientsBetweenDates(url, startDate, endDate) {
    console.log(`${url}/patientsBetweenDates?startDate=${startDate}&endDate=${endDate}`);
    let res = await fetch(`${url}/patientsBetweenDates?startDate=${startDate}&endDate=${endDate}`);
    
    if (res === undefined || res === null) {
        return null;
    }

    let data = await res.json();

    console.log(data.rows)
    return data.rows;
}

async function PostPatient(url, { id, location, city_name, has_symptoms, severity }) {
    console.log("request sent " + JSON.stringify({ id, location, city_name, has_symptoms, severity }))

    let res = await fetch(`${url}/addPatient`, {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id, location, city_name, has_symptoms, severity })
    })

    console.log(res.status);

    let message = ""; 

    switch (res.status) {
        case 500:
            message = "Server Internal error";
            break;
        case 400:
            message = "Bad Request (unexpected)";
            break;
        case 201:
            message = "Success! Patient added";
            break;
        default:
            message = "Unexpected status";
            break;
    }

    return { status: res.status, message: message };
}

export { GetCitiesName, GetPatientsBetweenDates, PostPatient }