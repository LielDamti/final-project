import React from 'react';
import Drawer from './components/PersistentDrawer';
import './App.css';

function App() {
  return (
    <div className="App">
        <React.Fragment>
          <Drawer></Drawer>
        </React.Fragment>
    </div>
  );
}

export default App;
