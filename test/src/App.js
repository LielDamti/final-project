import React from 'react';
import './App.css';
import './Map';

function App() {
  return (
    <div className="App">
        <Map></Map>
    </div>
  );
}

export default App;
